// main.cpp
// main entry points

#include "sketch.h"
#include <Arduino.h>
#include <Wire.h>
#include <esp_log.h>
#include <WiFi.h>
#include <SPI.h>

// to blink or not to blink...
bool doBlinking = true;

// constants
const char* ssid = "PeePeePooPoo";
const char* password = "no";
// variables
String header;

// Set a web server on port 80
WiFiServer server(80);
// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0;
// TimeOutTime
const long timeoutTime = 10000;

/////////////////////////////////////////////////////////////////////////////
// utilities
void loading(const int listOfLED[9]) {
  // reset the LEDs
  for (short i = 0; i<= 9; i++){
    digitalWrite(listOfLED[i], LOW);
  }
  // one by one turn on all LEDs
  for (short i = 0; i<= 9; i++){
    digitalWrite(listOfLED[i], HIGH);
    vTaskDelay(1000);
  }
}

/*String printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case ENC_TYPE_WEP:
      return("WEP");
      break;
    case ENC_TYPE_TKIP:
      return("WPA");
      break;
    case ENC_TYPE_CCMP:
      return("WPA2");
      break;
    case ENC_TYPE_NONE:
      return("None");
      break;
    case ENC_TYPE_AUTO:
      return("Auto");
      break;
    default:
      return("Encryption type not found");
      break;
  }
}*/

void listNetworks(WiFiClient client) {
  // scan for nearby networks:
  client.println("<h1>** Scan Networks **</h1>");
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1) {
    client.println("<p>Couldn't get a wifi connection</p>");
    while (true);
  }

  // print the list of networks seen:
  client.print("<p>number of available networks:");
  client.println(numSsid+"</p>");
  client.println("<form>");
  client.println("  <ul>");
  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    client.print("    <li>");
    client.print("<input type='radio' value='"+WiFi.SSID(thisNet)+"' id='"+WiFi.SSID(thisNet)+"' name='ssid'>");
    client.print(" Name: ");
    client.print(WiFi.SSID(thisNet));
    client.print(" Signal: ");
    client.print(WiFi.RSSI(thisNet));
    client.print(" dBm");
    //client.print(printEncryptionType(WiFi.encryptionType(thisNet)));
    client.println("</li>");
  }
  client.println("  </ul>");
  client.println("  <input type='text' name='password' placeholder='Password'>");
  client.println("  <input type='submit' value='Submit'>");
  client.println("</form>");
}

void displayNormalPage(WiFiClient client) {
  client.println("<!DOCTYPE html><html>");
  client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  client.println("<link rel=\"icon\" href=\"data:,\">");
  // CSS to style the on/off buttons 
  // Feel free to change the background-color and font-size attributes to fit your preferences
  client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
  client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
  client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
  client.println(".button2 {background-color: #555555;}</style></head>");
  
  // Web Page Heading
  client.println("<body>");
  client.println(header);
  listNetworks(client);
  client.println("</body>");
}

void connectToWifiNew(char* ssidnew, char* passwordnew = ""){
  Serial.print("Connecting to ");
  Serial.println(ssidnew);
  WiFi.begin(ssidnew,passwordnew);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Wifi connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

// delay/yield macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis

int firmwareVersion = 100; // used to check for updates
#define ECHECK ESP_ERROR_CHECK_WITHOUT_ABORT

// IDF logging
static const char *TAG = "main";


/////////////////////////////////////////////////////////////////////////////
// arduino-land entry points

void setup() {
  Serial.begin(115200);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Wifi connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  server.begin();
  
} // setup

void loop() {
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime) {  // loop while the client's connected
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // Display the HTML web page
            displayNormalPage(client);
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            if (currentLine.substring(0,3) == "GET"){
              int indexOfSsid = currentLine.indexOf("ssid=")+5;
              int indexOfPassword = currentLine.indexOf("password=")+9;
              bool SsidPresent = indexOfSsid > 4;
              bool PasswordPresent = indexOfPassword > 8;
              int switchCase;
              Serial.println(indexOfSsid);
              Serial.println(indexOfPassword);

              if(SsidPresent && PasswordPresent){
                String SsidGiven = currentLine.substring(indexOfSsid,currentLine.indexOf("&")+1);
                String PasswordGiven = currentLine.substring(indexOfPassword,currentLine.indexOf(" HTTP")+1);
                char SsidConverted[32];
                char PasswordConverted[32];
                SsidGiven.toCharArray(SsidConverted,SsidGiven.length());
                PasswordGiven.toCharArray(PasswordConverted,PasswordGiven.length());
                Serial.println(SsidConverted);
                Serial.println(PasswordConverted);
                connectToWifiNew(SsidConverted, PasswordConverted);
              }else if(SsidPresent){
                String Ssid = currentLine.substring(indexOfSsid,currentLine.indexOf(" HTTP")+1);
                char* SsidConverted;
                Ssid.toCharArray(SsidConverted,Ssid.length());
                connectToWifiNew(SsidConverted);
              }else if(PasswordPresent){
              }else{
              }

            }
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
} // loop

// pio run -t upload -t monitor

/////////////////////////////////////////////////////////////////////////////
// if we're an IDF build define app_main
// (TODO probably fails to identify a platformio *idf* build)

#if ! defined(ARDUINO_IDE_BUILD) && ! defined(PLATFORMIO)
  extern "C" { void app_main(); }

  // main entry point
  void app_main() {
    // arduino land
    initArduino();
    setup();
    while(1)
      loop();
  } // app_main()

#endif
