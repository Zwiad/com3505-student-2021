// main.cpp
// main entry points

#include "sketch.h"
#include <Arduino.h>
#include <Wire.h>
#include <esp_log.h>
#include <WiFi.h>

// to blink or not to blink...
bool doBlinking = true;

// constants
const int green1 = 32;
const int green2 = 15;
const int green3 = 33;
const int yellow1 = 27;
const int yellow2 = 12;
const int yellow3 = 13;
const int red1 = 21;
const int red2 = 17;
const int red3 = 16;

const int LED[9] = {red3,red2,red1,yellow3,yellow2,yellow1,green3,green2,green1};
//  {green1,green2,green3,yellow1,yellow2,yellow3,red1,red2,red3};

// variables
const short systemState = 0;

/////////////////////////////////////////////////////////////////////////////
// utilities
void loading(const int listOfLED[9]) {
  // reset the LEDs
  for (short i = 0; i<= 9; i++){
    digitalWrite(listOfLED[i], LOW);
  }
  // one by one turn on all LEDs
  for (short i = 0; i<= 9; i++){
    digitalWrite(listOfLED[i], HIGH);
    vTaskDelay(1000);
  }
}

// delay/yield macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis

int firmwareVersion = 100; // used to check for updates
#define ECHECK ESP_ERROR_CHECK_WITHOUT_ABORT

// IDF logging
static const char *TAG = "main";


/////////////////////////////////////////////////////////////////////////////
// arduino-land entry points

void setup() {
  Serial.begin(115200);
  Serial.println("arduino started");
  Serial.printf("\nwire pins: sda=%d scl=%d\n", SDA, SCL);

  pinMode(BUILTIN_LED, OUTPUT);
  int numOfLED = sizeof(LED)/sizeof(*LED);
  for (short i = 0; i<= numOfLED; i++){
    pinMode(LED[i], OUTPUT);
  }
} // setup

void loop() {
  loading(LED);
} // loop

// pio run -t upload -t monitor

/////////////////////////////////////////////////////////////////////////////
// if we're an IDF build define app_main
// (TODO probably fails to identify a platformio *idf* build)

#if ! defined(ARDUINO_IDE_BUILD) && ! defined(PLATFORMIO)
  extern "C" { void app_main(); }

  // main entry point
  void app_main() {
    // arduino land
    initArduino();
    setup();
    while(1)
      loop();
  } // app_main()

#endif
