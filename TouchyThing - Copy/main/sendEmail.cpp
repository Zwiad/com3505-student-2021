#include "TouchyThing.h"

const char* host = "maker.ifttt.com";       // Host that receives GET request and sends email
const int httpsPort = 443;                  // Port of Host that receives GET request and sends email

WiFiClientSecure client;    // used to send a GET request and receive a response

void sendEmail(String value){   // number of button presses
    Serial.print("connecting to ");     // lines 9:39 adapted from https://iotdesignpro.com/projects/how-trigger-led-using-ifttt-and-esp32-email-notification
    Serial.println(host);               
    if (!client.connect(host, httpsPort)){
        Serial.println("connection failed");
        return;
    }

    String url = "/trigger/ESP32/with/key/fOjSSpLO3KeLdNkNsf3xK8a7R_xI-7Wks6KU5q5TEOZ";
    Serial.print("requesting URL: ");
    Serial.println(url);
    Serial.print("number of button presses: ");
    Serial.println(value);
    client.print(String("GET ") + url + "?value1=" + value + " HTTP/1.1\r\n" + // value to be received in email passed in URL
                "Host: " + host + "\r\n" +
                "User-Agent: BuildFailureDetectorESP32\r\n" +
                "Connection: close\r\n\r\n");
    Serial.println("request sent");
    while (client.connected()) {
        // \n indicates end of message
        String line = client.readStringUntil('\n');
        if (line == "\r") {
        Serial.println("headers received");
        break;
        }
    }
    String line = client.readStringUntil('\n');
    Serial.println("reply:");
    Serial.println("```");
    Serial.println(line);
    Serial.println("```");
    Serial.println("closing connection");
}