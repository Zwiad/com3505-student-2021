#ifndef THING_H
#define THING_H

#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include "WiFiClientSecure.h"
#include <HTTPClient.h>
#include <Update.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

// morseCode Exports
    // Variables&Constants
extern const int TIMEUNIT;
extern const int MORSEALPHABET[];

    // Methods
String base10ToBase3(int);
void blinkCharacter(char);
void blinkString(String);
void ledOn();
void ledOff();

// startAPServer Exports
#define dbg(b, s) if(b) Serial.print(s)
#define dln(b, s) if(b) Serial.println(s)
#define startupDBG  true
#define netDBG      true
#define ALEN(a) ((int) (sizeof(a) / sizeof(a[0]))) // only in definition scope!
#define GET_HTML(strout, boiler, repls) \
    getHtml(strout, boiler, ALEN(boiler), repls, ALEN(repls));
typedef struct { int position; const char *replacement; } replacement_t;

    // Variables&Constants
extern char MAC_ADDRESS[];
extern String apSSID;
extern AsyncWebServer webServer;
extern const char *boiler[];
    // Methods
void getMAC(char *);
void startAP();
void printIPs();
void startWebServer();
void getHtml(String& html, const char *[], int, replacement_t [], int);
void handleRoot();

// sendEmail Exports
    // Methods
void sendEmail(String);

#endif