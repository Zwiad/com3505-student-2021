#include "TouchyThing.h"

//variables
char MAC_ADDRESS[13]; // mac address of the esp
String apSSID;  // name of access point
AsyncWebServer webServer(80);  // webServer on port 80 handles requests
const char *boiler[] = { // boilerplate: constants & pattern parts of template
  "<html><head><title>",                                                // 0
  "default title",                                                      // 1
  "</title>\n",                                                         // 2
  "<meta charset='utf-8'>",                                             // 3

  // adjacent strings in C are concatenated:
  "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n"
  "<style>body{background:#FFF; color: #000; font-family: sans-serif;", // 4

  "font-size: 150%;}</style>\n",                                        // 5
  "</head><body>\n<h2>",                                                // 6
  "Welcome to Thing!",                                                  // 7
  "</h2>\n<p><a href='/'>Home</a>&nbsp;&nbsp;&nbsp;</p>\n",             // 8
  "</body></html>\n\n",                                                 // 9
};

// MAC address method from week 1 solution to exercise
// Grabs the mac address using getEfuseMac and converts it to a hex address with the order reversed
void getMAC(char *buf) { // the MAC is 6 bytes, so needs careful conversion...
  uint64_t mac = ESP.getEfuseMac(); // ...to string (high 2, low 4):
  char rev[13];
  sprintf(rev, "%04X%08X", (uint16_t) (mac >> 32), (uint32_t) mac);

  // the byte order in the ESP has to be reversed relative to normal Arduino
  for(int i=0, j=11; i<=10; i+=2, j-=2) {
    buf[i] = rev[j - 1];
    buf[i + 1] = rev[j];
  }
  buf[12] = '\0';
}

// creates an access point named TouchyThing-MACADDRESS
void startAP() {
    apSSID = String("TouchyThing-");
    getMAC(MAC_ADDRESS);
    apSSID.concat(MAC_ADDRESS);

    if (!WiFi.mode(WIFI_AP_STA))
        dln(startupDBG, "Failed to set WiFi mode");
    if (!WiFi.softAP(apSSID.c_str(), "complicatedPassword"))
        dln(startupDBG, "Failed to start soft AP");
    printIPs();
}

// prints to serial relevant information to the access point
void printIPs() {
    if(startupDBG) { // easier than the debug macros for multiple lines etc.
        Serial.print("AP SSID: ");
        Serial.print(apSSID);
        Serial.print("; IP address(es): local=");
        Serial.print(WiFi.localIP());
        Serial.print("; AP=");
        Serial.println(WiFi.softAPIP());
    }
    if(netDBG)
        WiFi.printDiag(Serial);
}

// defines functionality of webserver on specific requests and starts the webserver
void startWebServer() {
    webServer.on("/get", HTTP_GET, [](AsyncWebServerRequest *request){
      Serial.println("Message Received");
      String message = "N/A";
      if (request->hasParam("message")){
        message = request->getParam("message")->value();
      }
      blinkString(message);
      request->send(200, "text/plain", "Message Received");
    });
    dln(startupDBG, "HTTP server started");
}

// turn array of strings & set of replacements into a String
void getHtml(
  String& html, const char *boiler[], int boilerLen,
  replacement_t repls[], int replsLen
) {
  for(int i = 0, j = 0; i < boilerLen; i++) {
    if(j < replsLen && repls[j].position == i)
      html.concat(repls[j++].replacement);
    else
      html.concat(boiler[i]);
  }
}