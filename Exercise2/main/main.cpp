// main.cpp
// main entry points

#include "sketch.h"
#include <Arduino.h>
#include <Wire.h>
#include <esp_log.h>
#include <WiFi.h>

// to blink or not to blink...
bool doBlinking = true;

// constants
const int pushButton = 14;
const int externalLED = 32;

// variables
int buttonState = 0;

/////////////////////////////////////////////////////////////////////////////
// utilities

// delay/yield macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis

int firmwareVersion = 100; // used to check for updates
#define ECHECK ESP_ERROR_CHECK_WITHOUT_ABORT

// IDF logging
static const char *TAG = "main";


/////////////////////////////////////////////////////////////////////////////
// arduino-land entry points

void setup() {
  // IDF 4.3 Wire.setPins(SDA, SCL);
  // Wire.begin();


  Serial.begin(115200);
  Serial.println("arduino started");
  Serial.printf("\nwire pins: sda=%d scl=%d\n", SDA, SCL);

  pinMode(BUILTIN_LED, OUTPUT);
  pinMode(externalLED, OUTPUT);
  pinMode(pushButton, INPUT_PULLUP);
} // setup

void loop() {
  printf("\nahem, hello world\n");

  #ifdef ESP32
    String outputString = "ESP Board MAC Address: " + WiFi.macAddress() + "\n";
    printf("ESP32 is defined\n");
    Serial.print(outputString);
  #endif

  buttonState = digitalRead(pushButton);

  if(buttonState) {
    Serial.print("Button is NOT pressed\n");
  } else {
    Serial.print("Button is pressed\n");
  }

  if(doBlinking) {
    digitalWrite(BUILTIN_LED, HIGH);
    digitalWrite(externalLED, HIGH);
  }
  WAIT_SECS(2)
  if(doBlinking) {
    digitalWrite(BUILTIN_LED, LOW);
    digitalWrite(externalLED, LOW);
  }
  WAIT_SECS(2)
} // loop


/////////////////////////////////////////////////////////////////////////////
// if we're an IDF build define app_main
// (TODO probably fails to identify a platformio *idf* build)

#if ! defined(ARDUINO_IDE_BUILD) && ! defined(PLATFORMIO)
  extern "C" { void app_main(); }

  // main entry point
  void app_main() {
    // arduino land
    initArduino();
    setup();
    while(1)
      loop();
  } // app_main()

#endif
