#include "TouchyThing.h"

//constants
const int TIMEUNIT = 100;
const int MORSEALPHABET[] = {
  5, 67, 70, 22, 1, 43, 25, 40, 4, 53, 23, 49, 8,   // A,B,C,D,E,F,G,H,I,J,K,L,M
  7, 26, 52, 77, 16, 13, 2, 14, 41, 17, 68, 71, 76, // N,O,P,Q,R,S,T,U,V,W,X,Y,Z
  161, 134, 125, 122, 121, 202, 229, 238, 241, 242, // 1,2,3,4,5,6,7,8,9,0
  455, 692, 715, 400, 484, 608, 205, 644, 644, 448       // . , : ? ' - / () ""
};
const byte VALUEOFPUNCTUATION[] = {
  46, 44, 58, 63, 39, 45, 47, 40, 41, 34
};
const byte SIZEOFVALUEOFPUNCTUATION = sizeof(VALUEOFPUNCTUATION);

void ledOn()  {digitalWrite(BUILTIN_LED, HIGH);}
void ledOff() {digitalWrite(BUILTIN_LED, LOW);}

// convert char to ascii value, then calculate the correct morse signal which is stored in MORSEALPHABET
void blinkCharacter(char character){
  ledOff();
  // conversion from char to byte
  byte asciiCharacter = character;
  byte positionInAlphabet;
  if (asciiCharacter >= 48  && asciiCharacter <= 57){ // if between 48 and 57, its a number
    positionInAlphabet = asciiCharacter - 22;
  } else if (asciiCharacter >= 65  && asciiCharacter <= 90){  // if between 65 and 90, its a letter 
    positionInAlphabet = asciiCharacter - 65;
  } else {  // could be punctuation. Since order of the symbols in ascii is different from morse
    for (byte i = 0; i <SIZEOFVALUEOFPUNCTUATION; i++){
      if (asciiCharacter == VALUEOFPUNCTUATION[i]){
        positionInAlphabet = 36 + i; // positionInAlphabet = position before first punctuation element plus i
      }
    }
  }

  // now that we have the character, blink the onboard LED
  if (positionInAlphabet){
    byte integer = MORSEALPHABET[positionInAlphabet];
    String signals = base10ToBase3(integer);
    // we are using base 3 to represent morse code where 1 is a short signal and 2 is a long signal.
    // maybe theres a way we could use base 2 but there is the problem of leading zeros being lost.
    for (auto signal : signals){
      switch(signal){
        case '1':
          ledOn();delay(TIMEUNIT);ledOff();
          break;
        case '2':
          ledOn();delay(TIMEUNIT*3);ledOff();
          break;
        default:
          return; // something wrong has happened if this is ran.
      }
      // delay between each signal is one TIMEUNIT. if last signal was transmitted, this delay is added
      // to the delay between characters, and finally delay between words.
      delay(TIMEUNIT);
    }
    delay(TIMEUNIT*3);
  }
}

// call blinkCharacter for each character in string and add necessary delay to comply with morse code
void blinkString(String toBlink){
  Serial.println(toBlink);
  for (auto character: toBlink){
    switch(character){
      case ' ':
        // space between words is a delay of 4*TIMEUNIT
        delay(TIMEUNIT*4);
        break;
      default:
        blinkCharacter(character);
    }
  }
}

// we have to convert from int to String and from base 10 to base 3. This function does both.
String base10ToBase3(int integer){
  String returnString = "";
  do {
    int value = integer / 3;
    byte remainder = integer % 3;
    integer = value;
    returnString = remainder+returnString;
  } while (integer != 0);
  return returnString;
}