# ProjectThing Documentation
## Author: Adam Tomaszewski
## Lab Assessment 2 (LA2)

### Summary
You may be familiar with my previous submission for the first Assessment for COM3505, namely a ESP that displays messages via Morse code. This submission for the second assessment incorporates said ESP, modified so it instead receives the message to print via a GET request. Also used in my project is the T-Watch 2020 which has had two functions implemented by me. The first is a Morse code decoder. The second allows the user of the watch to make a GET request with a custom message for the ESP to display via Morse code. 

### Disclaimer
Please note that while the code for translating messages into Morse code and displaying them was written by me, it was for a previous assignment and as such should be considered as written by a third party and not considered during the marking process. Similarly, I am using a base program for the T-Watch which allows for easy implementation of small applets, this code was written by someone else. To summarize, lines 483-783 in SimpleFramework.ino belong to me, and lines 21-51 in main.cpp were written by me.

Also, while I was previously using platformio to work on the ESP, I could not for the love of me get it working with the T-Watch, so after some reinstalling of libraries on a different computer I implemented the T-Watch code using base Arduino.

To view both the ESP and the T-Watch in action, visit this [link][0] to an unlisted YouTube video showing their different features.

![img](images\image1.jpg)

### Analysis of each feature and its testing

#### Receiving Morse Code Message
The ESP will display any message sent to it via a particular URL link, provided the device sending the message is on the same network. To do this I have decided to use the AsyncWebServer, modifying most of the code in main.cpp. The cool feature of an AsyncWebServer is that you don't need a handler in the loop to handle any requests. A downside to this, which I found after testing, is that the response to a request has to be quick as it's done asynchronously. This meant changing the behaviour somewhat so that now when a message was sent to the ESP it would be stored in a variable by the asynchronous function. Separately in the main loop I check the value of this global variable, and if it is not equal to "" I then display it using Morse code.

#### Sending Morse Code Message
The T-Watch has had 2 little applications written for it. The first application, Send Morse, will connect the user to a hard coded network. It will then display a keyboard allowing the user to type out the message. Pressing the orange button on the bottom sends the message. Since the list of available characters is 46 long, there was no way I could fit them all on one screen. The solution is to display only 6 characters at a time, and have arrow keys at the bottom to switch between screens. There are 8 screens in total, with numbers, punctuation and the space bar towards the end. Of course you can go straight to the end by going left from the first page, it loops around.

It took me a short while to verify that all the characters did indeed work, apart from the space bar. The reason was of course that you can not pass the space character into a url, so I switched the space bar to "%20". 

#### Translating Morse Code Message
The second application written for the T-Watch is the Morse Get app, which allows the user to translate a Morse code message. The interface for this is far cleaner, with only 5 buttons on one screen. The code behind this app is more complicated however than for Send Morse. The dot and dash represent the short and long signal respectively. Char represents the short pause between each character, and will attempt to interpret the currently input message as a character. If the user input cannot be translated, a * is added instead. The Word button does the same thing as Char, but adds a space after appending a character. This takes some getting used to, often at first when I would type out the last character I would press Char, only to then have to press Word to end the word, adding a star at the end of the word I was just typing out. Of course I could make it so if the Morse message is empty, Char and Word don't add a star at the end of the message, but I only just thought of that now.

The translated message appears on the next screen after pressing the bottom button. Yes, a long enough message will not display entirely as it will go out of the screen. A little fix for the future could be to make the page with the translated text scrollable, and maybe also to use the entire height of the screen.



***
[0]: https://www.youtube.com/watch?v=MnprPjATges	"IoT Assignment 2"
[]: 