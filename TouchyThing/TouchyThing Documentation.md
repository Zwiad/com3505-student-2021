# TouchyThing Documentation
## Author: Adam Tomaszewski
## Lab Assessment 1 (LA1)

### Summary
The IoT device pictured below implements 5 of the 6 tasks outlined in the first assessment, unfortunately I was not able to get OTA provisioning and update functions working in reasonable time. The code consists of multiple c++ files, where main.cpp is the workhorse of the device, startAPServer.cpp; morseCode.cpp and sendEmail.cpp include functionalities of which the device is capable, all of which is exported via and imported via a header file TouchyThing.h. The header file also contains references to external libraries used.

To view the ESP in action, visit this [link][0] to an unlisted youtube video showing the ESP's different features.

![img](images\image1.jpg)

- The device has the onboard LED configured to output a string passed to a function as Morse code.
- The device has 3 GPIO pins configured as touch sensor input. The purpose of the first two touch sensors is to move the device to a state, where each state has a specific string that is output via the onboard LED as Morse code. The 3rd touch sensor acts as a skip button, resulting in no message being output. This functionality represents the implementation of the "accepts control signals via the ESP32's touch sensing capability".
- The device has a fourth GPIO pin configured as input. The pressing of this button is detected via an interrupt attached during setup. The pressing of the button increments the number of times the button was pressed, and depending on whether this button was pressed while an email sending job was active or not, prints an appropriate message to Serial. This functionality represents the implementation of "adopts an appropriate event management strategy". The sending of the email, which includes the number of times the button was pressed at the time when the press that caused the email to be sent was pressed, is done via a WIFI Secure client which constructs a GET request to an IFTTT page which, upon receiving the request, sends an email to my inbox. This functionality represents the implementation of "responds to events via a cloud-based integration API such as that provided by IFTTT". Note, code to send the GET request has been adopted from this [tutorial][1].
- The device has 3 GPIO pins configured as output pins. The purpose of these pins is to represent which state the esp32 is in using LED's attached to the breadboard. This functionality, along with the configuration of the on-board LED, represent the implementation of "flashes LEDs to indicate different types of event".
- Hopefully this documentation, along with the video showing the device at work, and the extensive comments in the code, represent the implementation of "is well-documented at both technical and operational levels, including hardware photographs, a brief video of the system in use and a transcript of serial line diagnostic output".

### Analysis of each feature and its testing

#### LEDs displaying system state

The setup of the LEDs was fairly straightforward. Of course every time I add LEDs to a breadboard they go in the wrong way round and I have to flip them around. Once that was sorted, each LED that lights up corresponded to each of the three touch sensors that could be touched. There was a problem with some LEDs never turning off even after the system reset to state 0. The clearing of all LED values at the right time has fixed that. Note that the LEDs will sometimes persist after a finger is raised from a touch sensor. This is the intended behaviour, as it is likely the system is still in the state represented by the external LEDs while the Morse code for that specific state is being transmitted. In effect the state can only be changed by touching the touch sensors on the beginning of the loop, before the values of the touch sensors are read, and by continuing to touch them until they are read.

Touching the touch sensors before the firmware is flashed and during setup has no impact on the start-up of the system. Neither does quickly touching the sensors, most of the time it is just the case that the touch is not detected as by the time touchRead is called the sensor was released. Depending on which part of the finger and how hard the sensor is touched, sometimes the touch is not registered, specifically on the second sensor which is slightly smaller. Touching the wires is detected, but such a threshold was chosen such that this would have no effect on the state of the system. In hindsight this threshold could be raised slightly, or maybe even an algorithm to move the threshold according to the average value detected could be implemented. Another interesting thing to research would be the effect of the ambient temperature on what values are observed. Also of important note is that there was no effort to implement some sort of averaging over multiple samples to find an average to filter out any noise, but from observing the behaviour of the system, noise has never caused a false touch to be detected. Implementation of this would be trivial.

#### Morse Code
The implementation of the Morse code is somewhat complicated. To use as little space as possible, I've decided to store each character as a number which, when translated to base 3, forms a number who's digits can be interpreted as signals, with 1 being a short signal and 2 being a long signal. For Example, the letter B is stored as 67, which in base 3 is 2111, which equates to long, short, short, short, or .--- in Morse Code. I have tested the possible alphabet which includes all Capital letters, the digits from 0 to 9, and the punctuation . , : ? ' - / () "". Note that there is no way to discern between an open parentheses and a closed parentheses, hence the double occurrence of 644 in the constant MORSEALPHABET.
Once we calculate the correct signals to display, it is output via the on-board LED. The delays between the signals, and the alphabet itself, has been taken from [this website][2].

The function will accept any string, but no attempt is made to try and tidy it up before translation. If redundant spaces are added to the message anywhere, the ESP will cause a redundant delay where each occurrence of a space is treated as a space between two word. Any lowercase letters will not be accepted and will simply be skipped. Testing the lower case letters has shown a mistake where each 'skipped', i.e. invalid character would still apply the three TIMEUNITs delay between each character. The default case of the switch in blinkString() now has no delay, which was moved to inside blinkCharacter() where if the character is invalid, no delay is applied.

#### AP Server
The AP server is not really interacted with in any way, I don't know why I added it. But it's there, and the code used was mostly from the solutions to the exercises from week 1 (MAC Address) and week 6-7 (AP Server). When provided with the correct credentials it will connect to a network, and regardless of whether it does, it is possible to connect to the access point itself.
#### Send Email
This function sends an email to my inbox whenever the button is pressed. Not to worry, I have created a rule in said inbox to move all emails generated by the ESP straight to a separate folder after littering my inbox with spam mail. The triggering of the function is event based, i.e. the event of the button being pressed down causes a FALLING interrupt, which is almost always only triggered once. Earlier implementations used the ONLOW interrupt which is an event generated every time the button is detected as still being pressed, which would cause a short press to generate upwards of 30 emails. Fortunately by then a system already implemented to prevent the sending of an email while another email was waiting to be sent, and I imagine that in the case it wasn't, the huge number of GET requests would have caused a time out on any requests being accepted for some time by [IFTTT][3]. The previous link will take you to the IFTTT app which handles this process.

Another problem I ran into was that the number of times the button was pressed was tied to the button and thus could be incremented after the system flagged that an email should be sent in the next loop, but before it was actually sent. This was fixed with a new variable which was set to the value of the number of button presses when the flag to send an email was flipped to true.

The button has been tested extensively, pressing the button repeatedly from when the firmware is flashed onto the ESP, to during the sending of an email. Initial implementation attaches the interrupt at the beginning of setup and caused the ESP to restart during setup when pressed at that stage. I have since attached the interrupt further down, and was unable to replicate this behaviour.


***
[0]: https://youtu.be/kGQNkAaGDBg
[1]: https://iotdesignpro.com/projects/how-trigger-led-using-ifttt-and-esp32-email-notification
[2]: http://www.sckans.edu/~sireland/radio/code.html
[3]: https://ifttt.com/applets/qfjV5dxK
