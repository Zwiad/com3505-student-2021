#include "TouchyThing.h"

int firmwareVersion = 1;    // For use with OTA, unfortunately could not get it to work

// Variables
String messageToMorse = "";

// Constants
AsyncWebServer server(80);
const char* wifiSsid = "PeePeePooPoo";      // Wifi SSID
const char* wifiPassword = "736a7c3736";    // Wifi Key

void setup(){
    Serial.begin(115200);   // Serial Line init
    getMAC(MAC_ADDRESS);    // Get Mac Address, taken from week 1 exercise, declared in startAPServer.cpp
    pinMode(BUILTIN_LED, OUTPUT);   // Setup GPIO for built-in LED

    Serial.println(MAC_ADDRESS);
    Serial.println("```");

    WiFi.begin(wifiSsid, wifiPassword);
    Serial.print("Connecting to WiFi..");
 
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print(".");
    }

    Serial.println(WiFi.localIP());

    server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request){
      Serial.println("Message Received");
      String message = "N/A";
      if (request->hasParam("message")){
        message = request->getParam("message")->value();
      }
      messageToMorse = message;
      request->send(200, "text/plain", "Message Received");
    });
    dln(startupDBG, "HTTP server started");

    server.begin();
}

void loop(){
    if (messageToMorse != ""){
        blinkString(messageToMorse);
        messageToMorse = "";
    }
}
